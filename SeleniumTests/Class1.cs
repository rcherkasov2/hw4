﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;

namespace SeleniumTests
{
    
    [TestFixture]
    public class Class1
    {
        public IWebDriver driver = new ChromeDriver(@"D:\Chrome driver");
           
        [SetUp]
        public void Setup()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://wizzair.com/");
        }

        [TearDown]
        public void TearDown()
        {
            // Закрытие браузера
            driver.Quit();
        }

        [Test]
        public void WizzairTest()
        {
            // Открытие браузера, переход на сайт
          /*  IWebDriver driver = new ChromeDriver(@"D:\Chrome driver");
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://wizzair.com/");*/
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);

            // Закрытие предупреждения, о cookies
            driver.FindElement(By.XPath("//button[@class='cookie-policy__button']")).Click();

            // Создание объектов для полей ввода пункта отправления и назначения
            IWebElement inputOrigin = driver.FindElement(By.Id("search-departure-station"));
            IWebElement inputDestination = driver.FindElement(By.Id("search-arrival-station"));
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);


            // Очистка поля ввода и ввод пункта отправления
            inputOrigin.Clear(); //
            inputOrigin.SendKeys("Kiev");

            //Выбор первого варианта из выпадающего списка
            driver.FindElement(By.XPath("//label[@class='locations-container__location']")).Click();
            
            // Запись названия пункта отправления
            IWebElement OriginName = driver.FindElement(By.XPath("(//div[@class='rf-input__location rf-input__location--hint']/span)[1]"));
            IWebElement OriginIndex = driver.FindElement(By.XPath("(//div[@class='rf-input__location rf-input__location--hint']/span)[2]"));
            string originCorrectName = OriginName.Text;
            string originCorrectNameBrackets = $"{originCorrectName} ({OriginIndex.Text})";

            // Очистка поля ввода и ввод пункта назначения
            inputDestination.Clear();
            inputDestination.SendKeys("Copenhagen");

            //Выбор первого варианта из выпадающего списка
            driver.FindElement(By.XPath("//label[@class='locations-container__location']")).Click();

            // Запись названия пункта назначения
            IWebElement DestinationName = driver.FindElement(By.XPath("(//div[@class='rf-input__location rf-input__location--hint']/span)[3]"));
            IWebElement DestinationIndex = driver.FindElement(By.XPath("(//div[@class='rf-input__location rf-input__location--hint']/span)[4]"));
            string destinationCorrectName = DestinationName.Text;
            string destinationCorrectNameBrackets = $"{destinationCorrectName} ({DestinationIndex.Text})";

            // Запись даты полёта
            // wait.Until(dr => driver.FindElement(By.Id("search-departure-date")));
            IWebElement departueDateOrigin = driver.FindElement(By.Id("search-departure-date"));
            string departueDateTextCorrect = departueDateOrigin.Text;

            // Запись информации о дате возвращения (билет в один конец)
            IWebElement returnDate = driver.FindElement(By.XPath("//div[@id='search-return-date']"));
            string returnDateText = returnDate.Text;

            //Количество пассажиров
            IWebElement numberOfPassangers = driver.FindElement(By.XPath("//div[@id='search-passenger']"));
            string numberOfPassangersText = numberOfPassangers.Text;

            // Нажатие на кнопку "Search"
            driver.FindElement(By.XPath("//button[@tabindex='2']")).Click();

            // Проверка переключения на новую вкладку
            try
            {
                driver.SwitchTo().Window(driver.WindowHandles[1]); // новая вкладка открылась - переключение "фокуса" на неё
            }
            catch (Exception)
            {
                driver.SwitchTo().Window(driver.WindowHandles[0]); //  новая вкладка НЕ открылась - продолжение работы на изначальной вкладке
            }

            // проверка, есть ли сообщение о cookie на новой вкладке

            bool displayedCookies = driver.FindElement(By.XPath("//button[@class='cookie-policy__button']")).Displayed;
            if (displayedCookies)
            {
                driver.FindElement(By.XPath("//button[@class='cookie-policy__button']")).Click();
            }

            //-----------------------------------НОВАЯ ВКЛАДКА-------------------------------------------------------------

            // Запись маршрута для проверки с ожиданием с центра страницы на новой вкладке, рядом с выбором цены билета
            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(dr => driver.FindElement(By.XPath("//*[contains(@class,'4 date')]")));
            IWebElement departueDateNewCentr = driver.FindElement(By.XPath("//*[contains(@class,'4 date')]"));
            string departueDateForCheckCentr = departueDateNewCentr.Text;
            
            // Проверка 
            Assert.That(departueDateForCheckCentr, Does.Contain(departueDateTextCorrect));

            // Запись маршрута для проверки с страницы на новой вкладке из выбора билетов в один конец
            IWebElement originNameNewOneWay = driver.FindElement(By.XPath("//*[contains(@class,'booking-flow__flight-select__title heading heading--3')]"));
            string originNameForCheckBracketsOneWay = originNameNewOneWay.Text;

            // Проверка
            string fullAddressWithoutHyphen = originCorrectNameBrackets + " " + destinationCorrectNameBrackets;
            Assert.That(originNameForCheckBracketsOneWay, Does.Contain(fullAddressWithoutHyphen));

            // Проверка отображения трёх ценовых предложений
            IList<IWebElement> threeVariantsOfCost = driver.FindElements(By.XPath("//div[@class='rf-fare']"));
            Assert.That(threeVariantsOfCost.Count == 3);
           
            // Выбор "basic" билета
            IWebElement ticketBasicCheckbox = driver.FindElement(By.XPath("//div[@class='rf-fare__price']"));
            ticketBasicCheckbox.Click();

            // Запись маршрута возле лейбла в левом верхенем углу для проверки
            IWebElement originNameNewLeftTopLabel = driver.FindElement(By.XPath("//div[@class='booking-flow__itinerary__route']"));
            string originNameForCheckBracketsNewLeftTopLabel = originNameNewLeftTopLabel.Text;
            string fullAddressWithHyphen = originCorrectName + " — " + destinationCorrectName;

            // Проверка 
            Assert.That(originNameForCheckBracketsNewLeftTopLabel, Does.Contain(fullAddressWithHyphen.ToUpper()));

            // Запись маршрута для проверки с страницы на новой вкладке из выбора обратных билетов 
            IWebElement originNameNewBackTickets = driver.FindElement(By.XPath("(//*[contains(@class,'booking-flow__flight-select__title heading heading--3')])[2]"));
            string originNameForCheckBracketsBackTickets = originNameNewBackTickets.Text;
            string fullAddressWithoutHyphenRevers = destinationCorrectNameBrackets + " " + originCorrectNameBrackets;

            // Проверка 
            Assert.That(originNameForCheckBracketsBackTickets, Does.Contain(fullAddressWithoutHyphenRevers));

            // НАжатие кнопки "continue"
            IWebElement ContinueButton = driver.FindElement(By.Id("flight-select-continue-btn"));
            ContinueButton.Click();

            //Запись маршрута для проверки с страницы на новой вкладке, слева, возле списка пассажиров
            IWebElement originNameNewLeftPassengers = driver.FindElement(By.XPath("//p[@class='booking-flow__itinerary__step__content__title title title--3']"));
            string originNameForCheckBracketsLeftPassengers = originNameNewLeftPassengers.Text;
            
            // Проверка
            Assert.That(originNameForCheckBracketsLeftPassengers, Does.Contain(originCorrectName.ToUpper())& Does.Contain(destinationCorrectName.ToUpper()));

            // Введение имени и фамилии пасажира
            IWebElement inputFirstName = driver.FindElement(By.Id("passenger-first-name-0"));
            inputFirstName.SendKeys("Vasiliy");

            IWebElement inputLastName = driver.FindElement(By.Id("passenger-last-name-0"));
            inputLastName.SendKeys("Pupkin");

            // Выбор пола
            driver.FindElement(By.XPath("//label[contains(@for, 'passenger-gender-0-male')]")).Click();

            // Выбор размера багажа
            driver.FindElement(By.CssSelector(".baggage-switcher--0")).Click();

            // Нажатие на кнопку "continue" с ожиданием
            wait.Until(dr => driver.FindElement(By.Id("passengers-continue-btn")));
            IWebElement cont = driver.FindElement(By.Id("passengers-continue-btn"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(cont).Perform();
            driver.FindElement(By.Id("passengers-continue-btn")).Click();
            
            // Проверка отображения окна регистрации
            Assert.True(driver.FindElement(By.Id("login-modal")).Displayed);

            
        }

    }
}
